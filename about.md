---
layout: page
title: About RISE
permalink: /about/
---

#### Goals of RISE Workshop

Welcome to the first (virtual) RISE (netwoRkIng SystEms) summer school. The main goals of RISE are
* to expand students' horizon and train the next generation of students on
  topics at the intersection of networking and systems, with an emphasis on future Internet technologies;
* to provide opportunities to explore what the future of Internet entails that
  students would not otherwise be exposed to during the traditional classes;
* to provide hands-on training on deploying real-world applications in a geographically
  distributed multi-data center cloud setting and understanding the implications;
* to understand the tools used for managing and monitoring systems in 
  data center; and
* to facilitate collaboration and networking opportunities among students and speakers.

#### Schedule?

The first summer school will be a 5-day event with 2-hours of classes + hands-on
session. All the sessions will be online through Zoom or WebEx.

See [here](https://onrg.gitlab.io/rise/schedule/) for more details about the
schedule.

#### How to Register?

[Register Here](https://docs.google.com/forms/d/10E5o3-uhDIDR_m5qvkpOFfoNQstVscftTxltnAIKOuI/edit?ts=5eb31eb9)

#### About RISE team

[Ram Durairajan](https://ix.cs.uoregon.edu/~ram/) is an Assistant Professor in the [Department of Computer and Information Science](https://cs.uoregon.edu/) at the [University of Oregon](https://www.uoregon.edu/) where he co-leads the Oregon Networking Research Group ([ONRG](https://onrg.gitlab.io/)). Ram earned his Ph.D. and M.S. degrees in Computer Sciences from the University of Wisconsin - Madison and his B.Tech. in Information Technology from the College of Engineering, Guindy (CEG), Anna University. Ram's research vision is to create a "robust Internet" by taking a measure-and-then-build approach. To this end, Ram is interested in (a) developing new techniques and tools to empirically measure issues, properties and characteristics of the Internet; and (b) building decision-support systems to enhance Internet robustness by mitigating risks, outages, and cybersecurity issues identified using empirical measurements. Ram's research has been recognized with NSF CRII award, UO faculty research award, best paper awards from ACM CoNEXT and ACM SIGCOMM GAIA, and has been covered in several fora (NYTimes, MIT Technology Review, Popular Science, Boston Globe, Gizmodo, Mashable, among others). 

[Sudarsun Kannan](https://www.cs.rutgers.edu/~sk2113/) is an Assistant Professor
at Rutgers University's Computer Science Department with a research focus on
Operating Systems. More specifically, Sudarsun works on problems relating to
heterogeneous resource (memory, storage, and compute) management challenges and
understanding their impact on large-scale applications. Before joining Rutgers,
Professor Kannan was a postdoc at the University of Wisconsin-Madison's Computer
Science Department and graduated from the College of Computing, Georgia Tech.
His thesis explored methods to support hardware heterogeneity in Operating
Systems. Sudarsun's research focus is at the intersection of hardware and
software, building operating systems and system software for next-generation
memory and storage technologies. Results from his work have appeared at premier
operating systems and architecture venues, including EuroSys, FAST, ISCA, HPCA,
PACT, IPDPS, and others. Sudarsun is also a recepient of NSF CNS CORE grant.
Sudarsun has taught several graduate and undergraduate-level courses and is also
a faculty mentor for Rutgers SAS Honors college.

