---
layout: page
title: Schedule
permalink: /schedule/
---
<div class="schedule">


<!--h1 class="page-heading">Posts</h1-->

<style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;}
.tg td{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  overflow:hidden;padding:10px 5px;word-break:normal;}
.tg th{border-color:black;border-style:solid;border-width:1px;font-family:Arial, sans-serif;font-size:14px;
  font-weight:normal;overflow:hidden;padding:10px 5px;word-break:normal;}
.tg .tg-0pky{border-color:inherit;text-align:left;vertical-align:top}
</style>

<a href="https://docs.google.com/forms/d/10E5o3-uhDIDR_m5qvkpOFfoNQstVscftTxltnAIKOuI/edit?ts=5eb31eb9">Register Here!</a>


  <table class="tg">
<thead>
  <tr>
    <th class="tg-0pky">Dates</th>
    <th class="tg-0pky">Hour 1 (12:00pm - 1:00pm ET)</th>
    <th class="tg-0pky">Hour 2 (1:00pm - 2:00pm ET)</th>
    <th class="tg-0pky">Instructor</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0pky">07/06/2020 </td>
    <td class="tg-0pky">Basics of Machine Infrastructure and System Setup</td>
    <td class="tg-0pky">Hands On</td>
    <td class="tg-0pky">Sudarsun</td>
  </tr>
  <tr>
    <td class="tg-0pky">07/07/2020</td>
    <td class="tg-0pky">Basics of Internet Measurements and Cloud Computing </td>
    <td class="tg-0pky">Hands On</td>
    <td class="tg-0pky">Ram</td>
  </tr>
  <tr>
    <td class="tg-0pky">07/08/2020</td>
    <td class="tg-0pky">Distributed Applications - Cassandra Case Study</td>
    <td class="tg-0pky">Hands On</td>
    <td class="tg-0pky">Sudarsun</td>
  </tr>
  <tr>
    <td class="tg-0pky">07/09/2020</td>
    <td class="tg-0pky">Deploying Applications</td>
    <td class="tg-0pky">Hands On</td>
    <td class="tg-0pky">Ram</td>
  </tr>
  <tr>
    <td class="tg-0pky">07/10/2020</td>
    <td class="tg-0pky"><a href="https://www.flux.utah.edu/paper/maricq-osdi18">Paper 1</a>, <a href="ttps://www.flux.utah.edu/paper/uta-nsdi20">Paper 2</a></td>
    <td class="tg-0pky">Hands-on with the statistics and experiment planning tools</td>
    <td class="tg-0pky">Rob Ricci and Dmitry Duplyakin</td>
  </tr>
</tbody>
</table>

<br>Lecture materials will be availabile in the Piazza page <br>


</div><br>

<!--
|       Day      |                 Hour 1 (12:00 pm - 1:00 pm)         | Hour 2 (1:00 pm - 2:00 pm)  |  Instructor  |
|----------------|-----------------------------------------------------|-----------------------------|--------------|
| 07/01/2020     | Basics of Machine Infrastructure and System Setup   |          Hands On           |   Sudarsun   |
| 07/02/2020     | Basics of Internet Measurements and Cloud Computing |          Hands On           |      Ram     |
| 07/03/2020     | Distributed Applications - Cassandra Case Study     |          Hands On           |   Sudarsun   |
| 07/04/2020     | Deploying Applications                              |          Hands On           |      Ram     |
| 07/05/2020     | Industry Speaker 1                                  |      Industry Speaker 2     |              |
-->


#### Meeting Mode
The summer school will be online this year. 

We will use Piazza for questions. Please [sign up](https://piazza.com/rutgers/summer2020/summerschool)!

Please see Piazza for Zoom meeting and password information.

#### Infrastructure
RISE summer school is geared towards deploying applications in geographically distributed data centers connected through a wide-area network. For hands-on infrastructure, we will use [NSF CloudLab
infrastructure](cloudlab.us). We will provide more details about getting access to the CloudLab infrastructure as we move closer to the summer school dates.
