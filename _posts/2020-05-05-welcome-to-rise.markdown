---
layout: post
title:  "Welcome to RISE!"
date:   2020-05-05 15:32:14 -0300
categories: jekyll update
---

Welcome to first (virtual) RISE (netwoRkIng SystEms) summer school. Check out the [goals of RISE](https://onrg.gitlab.io/rise/about/) and [schedule](https://onrg.gitlab.io/rise/schedule/). 

[Registration](https://docs.google.com/forms/d/10E5o3-uhDIDR_m5qvkpOFfoNQstVscftTxltnAIKOuI/edit?ts=5eb31eb9) is now open! 